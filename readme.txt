
1. Search Twitter API with following parameters

Documentation link : https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets

url we call : https://api.twitter.com/1.1/search/tweets.json

parameters: ["count" => 100, 'lang' => 'ar', 'q' => 'أ OR ب OR ج', 'result_type' => 'recent', 'geocode' => '25.25005,45.25324,180mi']

#count represent number of results we want from twitter api
#q represent words or letters we want to search, in this example we put few letters from arabic
#result_type - recent => we search recent tweets from twitter.
#geo_code - we put geocode of saudi arabia and number of miles we want to include the tweets posted from.


we will get result as json as following example format

{
  "statuses": [
    {
    "created_at": "Mon May 06 20:01:29 +0000 2019",
      "id": 1125490788736032770,
      "id_str": "1125490788736032770",
      "text": "Today's new update means that you can finally add Pizza Cat to your Retweet with comments! Learn more about this ne… https://t.co/Rbc9TF2s5X",
    }
    ],
    "search_metadata": {
      "completed_in": 0.047,
      "max_id": 1125490788736032770,
      "max_id_str": "1125490788736032770",
      "next_results": "?max_id=1124690280777699327&q=from%3Atwitterdev&count=2&include_entities=1&result_type=mixed",
      "query": "from%3Atwitterdev",
      "refresh_url": "?since_id=1125490788736032770&q=from%3Atwitterdev&result_type=mixed&include_entities=1",
      "count": 2,
      "since_id": 0,
      "since_id_str": "0"
    }
  }

  we will use next_results value for pagination to get next 100 tweets from Twitter API.


  Finally we will have set tweets and users, it will be in csv/tweets_from_search.csv and csv/users_from_search.csv

  2. we remove bot accounts by some factors

     => user profile description should be more than 10 characters.
     => user should have the location present.
     => we consider above two points, most of the bot or inactive accounts don't have description or location.
     => user should not posted more than 10 tweets, 25 followings per day based on user joined date and likes should be less than 50.
     => by this time we removed half of the users from the list. 40k to 10k.

     This step saves the data to csv/filtered_users.csv


  3. we check last user tweets.

    documentation link: https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline

    endpoint link: https://api.twitter.com/1.1/statuses/user_timeline.json

    => by doing this we can see how latest the tweets from the user is. some user only retweets the content or like the content.

    => we can get upto 200 tweets in single api call. 200 tweets is enough determine user status.

    => this stage filter user into csv/filtered_users_stage1.csv and it will contain additional five columns at end of the row.

    1. number of tweets from last 200 tweets; => 'tweets_from_last_200_statuses',
    2. number of retweets from last 200 tweets; => 'retweets_from_last_200_statuses',
    3. number of replies from last 200 tweets; => 'replies_from_last_200_statuses'
    4. count of repeated words from last 200 tweets => 'recurring_words_count'
    5. words with number of counts at last column, it will show only words have more than 10 counts. it's not accurate but it's used to determine whether the user tweet more about particular topic or not.

    at this step end we save users data to csv/filtered_users_stage1.csv and users last 200 tweets to filtered_users_tweets.csv


  4. in this step we compare the data we got from last step

     1. we can check if the user mention same username lot of times and check whether the user retweets too much compared to replies and tweets.
     2. by comparing it, if retweets or replies or tweets contains more than 50% value then we can consider that user bot or not a normal account.

     at this final step the users data saved to final_users_list.csv

     final users tweets will be in final_users_list_tweets_.csv
     final users retweets will be in  final_users_list_retweets_replies_.csv
